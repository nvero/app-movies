import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  private apiRoot:string = "https://run.mocky.io/v3/4f6a4ce1-3d2c-4adc-bf7e-114dd7c2d245";

  constructor(private http: HttpClient) { }

  getApi() {
    return this.http.get(this.apiRoot);
  }
}

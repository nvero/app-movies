import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  listmovies;

  constructor( private moviesService: MoviesService ) { }

  ngOnInit(): void {
    this.getMoviesList();
  }

  getMoviesList(): void {
    this.moviesService.getApi()
      .subscribe(
        response => this.listmovies = response['movies'],
        error => (console.log('Ups! we have an error: ', error))        
      );
  }
}
